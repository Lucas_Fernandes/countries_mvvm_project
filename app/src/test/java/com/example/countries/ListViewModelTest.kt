package com.example.countries

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.countries.model.CountriesService
import com.example.countries.model.Country
import com.example.countries.viewModel.ListViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

private const val COUNTRY_NAME = "countryName"
private const val CAPITAL = "capital"
private const val URL = "url"

class ListViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Mock
    lateinit var countriesService: CountriesService
    @InjectMocks
    var listViewModel = ListViewModel()

//    private var testSingle: Single<List<Country>>? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val immediate = object : Scheduler() {

            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }
    }

    @Test
    fun getCountriesFailure() {
        getCountryFailureMethodCall()

        assertEquals(true, listViewModel.countryLoadError.value)
        assertEquals(false, listViewModel.loading.value)
    }

    @Test
    fun getCountriesSuccess() {
        countriesSuccessMethodCall()

        assertEquals(1, listViewModel.countries.value?.size)
        assertEquals(false, listViewModel.countryLoadError.value)
        assertEquals(false, listViewModel.loading.value)
    }

    private fun countriesSuccessMethodCall() {
        //testSingle = Single.just(arrayListOf(Country(COUNTRY_NAME, CAPITAL, URL)))
        `when`(countriesService.getCountries())
            .thenReturn(Single.just(arrayListOf(Country(COUNTRY_NAME, CAPITAL, URL))))

        listViewModel.refresh()
    }

    private fun getCountryFailureMethodCall() {
        `when`(countriesService.getCountries()).thenReturn(Single.error(Throwable()))
        listViewModel.refresh()
    }
}