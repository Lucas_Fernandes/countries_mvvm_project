package com.example.countries.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.countries.R
import com.example.countries.getProgressDrawable
import com.example.countries.loadImage
import com.example.countries.model.Country
import kotlinx.android.synthetic.main.country_list_item.view.*

class CountryListAdapter(
    private val inflater: LayoutInflater,
    private val list: ArrayList<Country>) : RecyclerView.Adapter<CountryListAdapter.CountryViewHolder>() {

    fun updateCountries(newCountries: List<Country>) {
        list.apply {
            clear()
            addAll(newCountries)
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder =
        CountryViewHolder(inflater.inflate(R.layout.country_list_item, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) = holder.bind(list[position])

    inner class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(country: Country) {
            itemView.tvCountryName.text = country.countryName
            itemView.tvCountryCapital.text = country.capital
            itemView.ivCountryImage.loadImage(country.flag, getProgressDrawable(itemView.context))
        }
    }

}