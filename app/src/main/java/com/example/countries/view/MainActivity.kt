package com.example.countries.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.countries.R
import com.example.countries.viewModel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var viewModel: ListViewModel? = null
    private var countriesAdapter: CountryListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        countriesAdapter = CountryListAdapter(layoutInflater, arrayListOf())
        viewModel = ViewModelProvider(this@MainActivity).get(ListViewModel::class.java)
            .apply { refresh() }

        rvCountriesList.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = countriesAdapter
        }

        observeViewModel()

        swipeRefreshLayout.apply {
            setOnRefreshListener {
                isRefreshing = false
                viewModel!!.refresh()
            }
        }

    }

    private fun observeViewModel() {
        viewModel!!.apply {
            countries.observe(this@MainActivity, Observer { countries ->
                rvCountriesList.visibility = View.VISIBLE
                countries.let { countriesAdapter!!.updateCountries(it) }
            })

            countryLoadError.observe(this@MainActivity, Observer { isError ->
                isError.let { tvListError.visibility = if (it) View.VISIBLE else View.GONE }
            })

            loading.observe(this@MainActivity, Observer { isLoading ->
                isLoading.let {
                    pbLoadingView.visibility = if (it) {
                        tvListError.visibility = View.GONE
                        rvCountriesList.visibility = View.GONE
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
            })
        }
    }
}
